using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using href.Utils;
using System.Diagnostics;

namespace CharsetEncoder.Logic
{
    class CharsetEncodingCore
    {
        public delegate void InputFileLoaded();
        public event InputFileLoaded OnInputFileLoaded;
        
        public delegate void OutputFileSaved();
        public event OutputFileSaved OnOutputFileSaved;

        public CharsetEncodingCore()
        {
        }

        const int BufferSize = 8096;

        public void EncodeFile(string inputFileName, Encoding inputEncoding, string outputFileName, Encoding outputEncoding)
        {
            using (TextReader input = new StreamReader
              (new FileStream(inputFileName, FileMode.Open),
               inputEncoding))
            {
                // Open a TextWriter for the appropriate file
                using (TextWriter output = new StreamWriter
                       (new FileStream(outputFileName, FileMode.Create),
                        outputEncoding))
                {

                    // Create the buffer
                    char[] buffer = new char[BufferSize];
                    int len;

                    // Repeatedly copy data until we've finished
                    while ((len = input.Read(buffer, 0, BufferSize)) > 0)
                    {
                        output.Write(buffer, 0, len);
                    }
                }
            }
        }

        private DataTable CreateFolderDataTable()
        {
            DataTable table = new DataTable("FolderTable");
            table.Columns.Add("FileName");
            table.Columns.Add("OutputFileName");
            table.Columns.Add("InputEncoding");
            table.Columns.Add("OutputEncoding");
            return table;
        }

        public Encoding[] GetEncodings(string fileName)
        {
            string fileText = GetFileText(fileName);
            return EncodingTools.DetectOutgoingEncodings(fileText, EncodingTools.AllEncodings, true);
        }

        public DataTable GetEmptyEncodingsTable()
        {
            DataTable encodingTable = new DataTable("EncodingTable");
            encodingTable.Columns.Add("Encoding", typeof(Encoding));
            encodingTable.Columns.Add("WebName");
            return encodingTable;
        }

        public DataTable GetEncodingTable(string fileName)
        {
            Encoding[] encodings = GetEncodings(fileName);

            DataTable encodingTable = GetEmptyEncodingsTable();

            foreach (Encoding encoding in encodings)
            {
                DataRow row = encodingTable.NewRow();
                row["Encoding"] = encoding;
                row["WebName"] = encoding.WebName;
                encodingTable.Rows.Add(row);
            }
            return encodingTable;
        }

        public string GetFileText(string fileName)
        {
            string fileText = string.Empty;
            StreamReader reader = new StreamReader(fileName);
            fileText = reader.ReadToEnd();
            reader.Close();
            return fileText;
        }

        private int fFilesCount;
        private string fFolderName;

        public int GetFilesCount(string folderName, bool refresh)
        {
            if (fFolderName == folderName && !refresh)
            {
                return fFilesCount;
            }
            
            fFilesCount = 0;

            int filesCount = 0;
            if (!Directory.Exists(folderName))
                throw new DirectoryNotFoundException(string.Format("Directory not exist {0}",folderName));
            Stack<string> directories = new Stack<string>();
            directories.Push(folderName);
            while(directories.Count > 0)
            {
                string directory = directories.Pop();
                fFilesCount += Directory.GetFiles(directory).Length;
                string [] subDirecties = Directory.GetDirectories(directory);
                foreach (string subDirectory in subDirecties)
                {
                    directories.Push(subDirectory);
                }
            }

            return fFilesCount;
        }

        public DataTable GetFilesFromFolder(DataTable folderTable, string folderName)
        {
            if (!Directory.Exists(folderName))
                throw new DirectoryNotFoundException(string.Format("Directory {0} not found", folderName));
            if (folderTable == null)
                folderTable = CreateFolderDataTable();
            Stack<string> directories = new Stack<string>();
            directories.Push(folderName);
            while (directories.Count > 0)
            {
                string directoryName = directories.Pop();
                string [] files = Directory.GetFiles(directoryName);
                foreach (string file in files)
                {
                    DataRow row = folderTable.NewRow();
                    row["FileName"] = file;
                    row["OutputFileName"] = file.Replace(folderName+@"\","");
                    Encoding encForMime = EncodingTools.GetMostEfficientEncoding(GetFileText(file));
                    row["InputEncoding"] = encForMime.WebName;
                    row["OutputEncoding"] = encForMime.WebName;
                    folderTable.Rows.Add(row);
                    OnInputFileLoaded();
                }
                string [] subDirs = Directory.GetDirectories(directoryName);
                foreach (string dir in subDirs)
                {
                    directories.Push(dir);
                }
            }

            return folderTable;
        }

        public void AddFileAndEncodingToTable(ref DataTable table, string fileName)
        {
            if (table == null)
                table = CreateFolderDataTable();
            DataRow row = table.NewRow();
            row["FileName"] = fileName;
            row["OutputFileName"] = "";
            Encoding encForMime = null;
            try
            {
                encForMime = EncodingTools.GetMostEfficientEncoding(GetFileText(fileName));
            }
            catch (System.ArgumentException ex)
            {
                return;
            }

            row["InputEncoding"] = encForMime.WebName;
            row["OutputEncoding"] = encForMime.WebName;
            table.Rows.Add(row);
        }

        public DataTable GeAllEncodingsTable(DataTable folderTable)
        {
            DataTable encodingTable = GetEmptyEncodingsTable();
            Hashtable encodings = new Hashtable();
            foreach (DataRow row in folderTable.Rows)
            {
                string fileName = (string)row["FileName"];
                Encoding[] fileEncodings = GetEncodings(fileName);
                foreach (Encoding encoding in fileEncodings)
                {
                    if (!encodings.ContainsKey(encoding.WebName))
                    {
                        encodings.Add(encoding.WebName, encoding);
                    }
                }
            }

            foreach (string WebName in encodings.Keys)
            {
                DataRow row = encodingTable.NewRow();
                row["Encoding"] = encodings[WebName];
                row["WebName"] = WebName;
                encodingTable.Rows.Add(row);
            }

            return encodingTable;
        }

        public void SetOutputEncoding(DataTable filesTable, string outputEncoding)
        {
            foreach (DataRow row in filesTable.Rows)
            {
                row["OutputEncoding"] = outputEncoding;
            }
        }


        public void ResetOutputEncoding(DataTable filesTable)
        {
            foreach (DataRow row in filesTable.Rows)
            {
                row["OutputEncoding"] = row["InputEncoding"];
            }
        }

        public void ViewFile(string fileName)
        {
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(fileName);
            p.Start();
        }

        public Hashtable GetOutputDirs(DataTable filesTable, string outputDir)
        {
            Hashtable directories = new Hashtable();
            foreach (DataRow row in filesTable.Rows)
            {
                string file = (string)row["OutputFileName"];
                if (file.IndexOf(@"\") != -1)
                {
                    string dir = file.Substring(0, file.LastIndexOf(@"\"));
                    if (!directories.ContainsKey(dir))
                    {
                        DirectoryInfo d = new DirectoryInfo(outputDir + @"\" + dir);                        
                        directories.Add(dir,d);
                    }
                }
            }
            return directories;

        }

        public void CreateOutputDirs(DataTable filesTable, string outputDir)
        {
            Hashtable dirs = GetOutputDirs(filesTable, outputDir);
            foreach (string dirName in dirs.Keys)
            {
                DirectoryInfo info = (DirectoryInfo)dirs[dirName];
                info.Create();
            }
        }

        public void CreateOutputFiles(DataTable filesTable, string outputDir)
        {            
            foreach (DataRow row in filesTable.Rows)
            {
                string inputFileName = (string)row["FileName"];
                string outputFileName = outputDir + @"\"+ (string)row["OutputFileName"]; 
                Encoding inputEncoding = Encoding.GetEncoding((string)row["InputEncoding"]);
                Encoding outputEncoding = Encoding.GetEncoding((string)row["OutputEncoding"]);
                EncodeFile(inputFileName, inputEncoding, outputFileName, outputEncoding);
            }
        }

    }
}
