using System;
using System.Collections.Generic;
using System.Text;

namespace CharsetEncoder.Logic
{
    struct FileEntity
    {
        public string outputFileName;
        public Encoding outputEncoding;
    }
}
