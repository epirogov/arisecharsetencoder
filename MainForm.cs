using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using folderSelect;
using CharsetEncoder.Logic;

namespace CharsetEncoder
{
    public partial class MainForm : Form
    {
        private CharsetEncodingCore core;
        public MainForm()
        {
            InitializeComponent();
            
            core = new CharsetEncodingCore();
            core.OnInputFileLoaded += new CharsetEncodingCore.InputFileLoaded(core_OnInputFileLoaded);
            core.OnOutputFileSaved += new CharsetEncodingCore.OutputFileSaved(core_OnOutputFileSaved);
        }        

        

        DataTable fFilesTable;
        public void BindDir()
        {            
            dgvFiles.DataSource = fFilesTable;
        }
  
        private void dgvFiles_Click(object sender, EventArgs e)
        {
            string fileName = (string)fFilesTable.Rows[dgvFiles.CurrentRow.Index]["FileName"];
            lbCurrentInputFile.Text = fileName;
            string currentInputEncoding = (string)fFilesTable.Rows[dgvFiles.CurrentRow.Index]["InputEncoding"];
            DataTable encodingTable = core.GetEncodingTable(fileName);
            cbInputEncoding.DataSource = encodingTable;
            cbInputEncoding.ValueMember = "Encoding";
            cbInputEncoding.DisplayMember = "WebName";
            cbInputEncoding.Text = currentInputEncoding;
            
            string currentOutputEncoding = (string)fFilesTable.Rows[dgvFiles.CurrentRow.Index]["OutputEncoding"];            
            cbOutputEncoding.DataSource = encodingTable.Copy();
            cbOutputEncoding.ValueMember = "Encoding";
            cbOutputEncoding.DisplayMember = "WebName";
            cbOutputEncoding.Text = currentOutputEncoding;
        }

        private void cbInputEncoding_SelectedIndexChanged(object sender, EventArgs e)
        {
            fFilesTable.Rows[dgvFiles.CurrentRow.Index]["InputEncoding"] = cbInputEncoding.Text;
        }

        public void BindOutputEncoding()
        {
            cbOutputEncodingAll.DataSource = core.GeAllEncodingsTable(fFilesTable);
            cbOutputEncodingAll.ValueMember = "Encoding";
            cbOutputEncodingAll.DisplayMember = "WebName";
        }

        private void btnSetOutputEncoding_Click(object sender, EventArgs e)
        {
            core.SetOutputEncoding(fFilesTable, cbOutputEncodingAll.Text);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnResetEncoding_Click(object sender, EventArgs e)
        {
            core.ResetOutputEncoding(fFilesTable);
        }

        private void cbOutputEncoding_SelectedIndexChanged(object sender, EventArgs e)
        {
            fFilesTable.Rows[dgvFiles.CurrentRow.Index]["OutputEncoding"] = cbOutputEncoding.Text;
            
        }

        void core_OnInputFileLoaded()
        {
            pbInputFiles.Minimum = 0;            
            pbInputFiles.Maximum = core.GetFilesCount(tbDir.Text,false);
            pbInputFiles.Step = 1;
            if (pbInputFiles.Value <= pbInputFiles.Maximum)
                pbInputFiles.Value++;
        }

        void core_OnOutputFileSaved()
        {
            pbOutputFiles.Minimum = 0;
            pbOutputFiles.Maximum = core.GetFilesCount(tbDir.Text, false);
            pbOutputFiles.Step = 1;
            if (pbOutputFiles.Value <= pbOutputFiles.Maximum)
                pbOutputFiles.Value++;
        }        

        private void btnFileDelete_Click(object sender, EventArgs e)
        {
            fFilesTable.Rows[dgvFiles.CurrentRow.Index].Delete();
            fFilesTable.AcceptChanges();
        }

        private void btnInputFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                tbFile.Text = fileDialog.FileName;
            }
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            core.AddFileAndEncodingToTable(ref fFilesTable, tbFile.Text);
            BindDir();
        }

        private void btnBind_Click(object sender, EventArgs e)
        {
            fFilesTable = core.GetFilesFromFolder(fFilesTable, tbDir.Text);                
            BindDir();            
            BindOutputEncoding();
            pbInputFiles.Value = 0;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            core.ViewFile(fFilesTable.Rows[dgvFiles.CurrentRow.Index]["FileName"].ToString());
        }

        private void btnDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                string dir = folderDialog.SelectedPath;
                tbDir.Text = dir;
            }
        }

        private void dgvFiles_Click_1(object sender, EventArgs e)
        {

        }

        private void btnOutputDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                tbOutputDir.Text = folderDialog.SelectedPath;
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            core.CreateOutputDirs(fFilesTable, tbOutputDir.Text);
            core.CreateOutputFiles(fFilesTable, tbOutputDir.Text);
        }
        
    }
}