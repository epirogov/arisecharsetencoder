namespace CharsetEncoder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBind = new System.Windows.Forms.Button();
            this.btnInputFile = new System.Windows.Forms.Button();
            this.pbOutputFiles = new System.Windows.Forms.ProgressBar();
            this.pbInputFiles = new System.Windows.Forms.ProgressBar();
            this.cbOutputEncoding = new System.Windows.Forms.ComboBox();
            this.cbInputEncoding = new System.Windows.Forms.ComboBox();
            this.btnResetEncoding = new System.Windows.Forms.Button();
            this.btnSetOutputEncoding = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cbOutputEncodingAll = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbCurrentInputFile = new System.Windows.Forms.Label();
            this.btnView = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnOutputDir = new System.Windows.Forms.Button();
            this.lbOutputDir = new System.Windows.Forms.Label();
            this.tbOutputDir = new System.Windows.Forms.TextBox();
            this.lbFile = new System.Windows.Forms.Label();
            this.tbFile = new System.Windows.Forms.TextBox();
            this.btnFile = new System.Windows.Forms.Button();
            this.btnFileDelete = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvFiles = new System.Windows.Forms.DataGridView();
            this.btnDir = new System.Windows.Forms.Button();
            this.lbDir = new System.Windows.Forms.Label();
            this.tbDir = new System.Windows.Forms.TextBox();
            this.cbSeparateFolders = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBind
            // 
            this.btnBind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBind.Location = new System.Drawing.Point(516, 43);
            this.btnBind.Name = "btnBind";
            this.btnBind.Size = new System.Drawing.Size(44, 23);
            this.btnBind.TabIndex = 140;
            this.btnBind.Text = "Bind";
            this.btnBind.UseVisualStyleBackColor = true;
            this.btnBind.Click += new System.EventHandler(this.btnBind_Click);
            // 
            // btnInputFile
            // 
            this.btnInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInputFile.Location = new System.Drawing.Point(486, 308);
            this.btnInputFile.Name = "btnInputFile";
            this.btnInputFile.Size = new System.Drawing.Size(35, 23);
            this.btnInputFile.TabIndex = 139;
            this.btnInputFile.Text = "...";
            this.btnInputFile.UseVisualStyleBackColor = true;
            this.btnInputFile.Click += new System.EventHandler(this.btnInputFile_Click);
            // 
            // pbOutputFiles
            // 
            this.pbOutputFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOutputFiles.Location = new System.Drawing.Point(12, 405);
            this.pbOutputFiles.Name = "pbOutputFiles";
            this.pbOutputFiles.Size = new System.Drawing.Size(548, 23);
            this.pbOutputFiles.TabIndex = 138;
            // 
            // pbInputFiles
            // 
            this.pbInputFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbInputFiles.Location = new System.Drawing.Point(13, 72);
            this.pbInputFiles.Name = "pbInputFiles";
            this.pbInputFiles.Size = new System.Drawing.Size(548, 23);
            this.pbInputFiles.TabIndex = 137;
            // 
            // cbOutputEncoding
            // 
            this.cbOutputEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbOutputEncoding.FormattingEnabled = true;
            this.cbOutputEncoding.Location = new System.Drawing.Point(334, 268);
            this.cbOutputEncoding.Name = "cbOutputEncoding";
            this.cbOutputEncoding.Size = new System.Drawing.Size(121, 21);
            this.cbOutputEncoding.TabIndex = 136;
            this.cbOutputEncoding.SelectedIndexChanged += new System.EventHandler(this.cbOutputEncoding_SelectedIndexChanged);
            // 
            // cbInputEncoding
            // 
            this.cbInputEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbInputEncoding.FormattingEnabled = true;
            this.cbInputEncoding.Location = new System.Drawing.Point(104, 268);
            this.cbInputEncoding.Name = "cbInputEncoding";
            this.cbInputEncoding.Size = new System.Drawing.Size(121, 21);
            this.cbInputEncoding.TabIndex = 135;
            this.cbInputEncoding.SelectedIndexChanged += new System.EventHandler(this.cbInputEncoding_SelectedIndexChanged);
            // 
            // btnResetEncoding
            // 
            this.btnResetEncoding.Location = new System.Drawing.Point(462, 101);
            this.btnResetEncoding.Name = "btnResetEncoding";
            this.btnResetEncoding.Size = new System.Drawing.Size(99, 23);
            this.btnResetEncoding.TabIndex = 134;
            this.btnResetEncoding.Text = "Reset";
            this.btnResetEncoding.UseVisualStyleBackColor = true;
            this.btnResetEncoding.Click += new System.EventHandler(this.btnResetEncoding_Click);
            // 
            // btnSetOutputEncoding
            // 
            this.btnSetOutputEncoding.Location = new System.Drawing.Point(357, 101);
            this.btnSetOutputEncoding.Name = "btnSetOutputEncoding";
            this.btnSetOutputEncoding.Size = new System.Drawing.Size(99, 23);
            this.btnSetOutputEncoding.TabIndex = 133;
            this.btnSetOutputEncoding.Text = "Set Encoding";
            this.btnSetOutputEncoding.UseVisualStyleBackColor = true;
            this.btnSetOutputEncoding.Click += new System.EventHandler(this.btnSetOutputEncoding_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(143, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 132;
            this.label5.Text = "Output Encoding :";
            // 
            // cbOutputEncodingAll
            // 
            this.cbOutputEncodingAll.FormattingEnabled = true;
            this.cbOutputEncodingAll.Location = new System.Drawing.Point(242, 103);
            this.cbOutputEncodingAll.Name = "cbOutputEncodingAll";
            this.cbOutputEncodingAll.Size = new System.Drawing.Size(109, 21);
            this.cbOutputEncodingAll.TabIndex = 131;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(234, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 130;
            this.label4.Text = "Output Encoding :";
            // 
            // lbCurrentInputFile
            // 
            this.lbCurrentInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCurrentInputFile.AutoSize = true;
            this.lbCurrentInputFile.Location = new System.Drawing.Point(13, 246);
            this.lbCurrentInputFile.Name = "lbCurrentInputFile";
            this.lbCurrentInputFile.Size = new System.Drawing.Size(60, 13);
            this.lbCurrentInputFile.TabIndex = 129;
            this.lbCurrentInputFile.Text = "[File Name]";
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView.Location = new System.Drawing.Point(485, 266);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 128;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerate.Location = new System.Drawing.Point(487, 436);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 127;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnOutputDir
            // 
            this.btnOutputDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOutputDir.Location = new System.Drawing.Point(525, 353);
            this.btnOutputDir.Name = "btnOutputDir";
            this.btnOutputDir.Size = new System.Drawing.Size(35, 23);
            this.btnOutputDir.TabIndex = 126;
            this.btnOutputDir.Text = "...";
            this.btnOutputDir.UseVisualStyleBackColor = true;
            this.btnOutputDir.Click += new System.EventHandler(this.btnOutputDir_Click);
            // 
            // lbOutputDir
            // 
            this.lbOutputDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbOutputDir.AutoSize = true;
            this.lbOutputDir.Location = new System.Drawing.Point(10, 341);
            this.lbOutputDir.Name = "lbOutputDir";
            this.lbOutputDir.Size = new System.Drawing.Size(61, 13);
            this.lbOutputDir.TabIndex = 125;
            this.lbOutputDir.Text = "Output Dir :";
            // 
            // tbOutputDir
            // 
            this.tbOutputDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutputDir.Location = new System.Drawing.Point(13, 355);
            this.tbOutputDir.Name = "tbOutputDir";
            this.tbOutputDir.Size = new System.Drawing.Size(508, 20);
            this.tbOutputDir.TabIndex = 124;
            // 
            // lbFile
            // 
            this.lbFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbFile.AutoSize = true;
            this.lbFile.Location = new System.Drawing.Point(13, 294);
            this.lbFile.Name = "lbFile";
            this.lbFile.Size = new System.Drawing.Size(29, 13);
            this.lbFile.TabIndex = 123;
            this.lbFile.Text = "File :";
            // 
            // tbFile
            // 
            this.tbFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFile.Location = new System.Drawing.Point(13, 310);
            this.tbFile.Name = "tbFile";
            this.tbFile.Size = new System.Drawing.Size(466, 20);
            this.tbFile.TabIndex = 122;
            // 
            // btnFile
            // 
            this.btnFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFile.Location = new System.Drawing.Point(527, 307);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(35, 23);
            this.btnFile.TabIndex = 121;
            this.btnFile.Text = "Add";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // btnFileDelete
            // 
            this.btnFileDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFileDelete.Location = new System.Drawing.Point(485, 241);
            this.btnFileDelete.Name = "btnFileDelete";
            this.btnFileDelete.Size = new System.Drawing.Size(76, 23);
            this.btnFileDelete.TabIndex = 120;
            this.btnFileDelete.Text = "Delete";
            this.btnFileDelete.UseVisualStyleBackColor = true;
            this.btnFileDelete.Click += new System.EventHandler(this.btnFileDelete_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 119;
            this.label2.Text = "Input Encoding :";
            // 
            // dgvFiles
            // 
            this.dgvFiles.AllowUserToAddRows = false;
            this.dgvFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFiles.Location = new System.Drawing.Point(13, 130);
            this.dgvFiles.Name = "dgvFiles";
            this.dgvFiles.ReadOnly = true;
            this.dgvFiles.Size = new System.Drawing.Size(549, 110);
            this.dgvFiles.TabIndex = 118;
            this.dgvFiles.Click += new System.EventHandler(this.dgvFiles_Click);
            // 
            // btnDir
            // 
            this.btnDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDir.Location = new System.Drawing.Point(472, 43);
            this.btnDir.Name = "btnDir";
            this.btnDir.Size = new System.Drawing.Size(35, 23);
            this.btnDir.TabIndex = 117;
            this.btnDir.Text = "...";
            this.btnDir.UseVisualStyleBackColor = true;
            this.btnDir.Click += new System.EventHandler(this.btnDir_Click);
            // 
            // lbDir
            // 
            this.lbDir.AutoSize = true;
            this.lbDir.Location = new System.Drawing.Point(13, 29);
            this.lbDir.Name = "lbDir";
            this.lbDir.Size = new System.Drawing.Size(26, 13);
            this.lbDir.TabIndex = 116;
            this.lbDir.Text = "Dir :";
            // 
            // tbDir
            // 
            this.tbDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDir.Location = new System.Drawing.Point(13, 46);
            this.tbDir.Name = "tbDir";
            this.tbDir.Size = new System.Drawing.Size(453, 20);
            this.tbDir.TabIndex = 115;
            // 
            // cbSeparateFolders
            // 
            this.cbSeparateFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbSeparateFolders.AutoSize = true;
            this.cbSeparateFolders.Checked = true;
            this.cbSeparateFolders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSeparateFolders.Location = new System.Drawing.Point(13, 381);
            this.cbSeparateFolders.Name = "cbSeparateFolders";
            this.cbSeparateFolders.Size = new System.Drawing.Size(140, 17);
            this.cbSeparateFolders.TabIndex = 141;
            this.cbSeparateFolders.Text = "Create Separate Folders";
            this.cbSeparateFolders.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 463);
            this.Controls.Add(this.cbSeparateFolders);
            this.Controls.Add(this.btnBind);
            this.Controls.Add(this.btnInputFile);
            this.Controls.Add(this.pbOutputFiles);
            this.Controls.Add(this.pbInputFiles);
            this.Controls.Add(this.cbOutputEncoding);
            this.Controls.Add(this.cbInputEncoding);
            this.Controls.Add(this.btnResetEncoding);
            this.Controls.Add(this.btnSetOutputEncoding);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbOutputEncodingAll);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbCurrentInputFile);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.btnOutputDir);
            this.Controls.Add(this.lbOutputDir);
            this.Controls.Add(this.tbOutputDir);
            this.Controls.Add(this.lbFile);
            this.Controls.Add(this.tbFile);
            this.Controls.Add(this.btnFile);
            this.Controls.Add(this.btnFileDelete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvFiles);
            this.Controls.Add(this.btnDir);
            this.Controls.Add(this.lbDir);
            this.Controls.Add(this.tbDir);
            this.Name = "MainForm";
            this.Text = "Charset Encoder";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBind;
        private System.Windows.Forms.Button btnInputFile;
        private System.Windows.Forms.ProgressBar pbOutputFiles;
        private System.Windows.Forms.ProgressBar pbInputFiles;
        private System.Windows.Forms.ComboBox cbOutputEncoding;
        private System.Windows.Forms.ComboBox cbInputEncoding;
        private System.Windows.Forms.Button btnResetEncoding;
        private System.Windows.Forms.Button btnSetOutputEncoding;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbOutputEncodingAll;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbCurrentInputFile;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnOutputDir;
        private System.Windows.Forms.Label lbOutputDir;
        private System.Windows.Forms.TextBox tbOutputDir;
        private System.Windows.Forms.Label lbFile;
        private System.Windows.Forms.TextBox tbFile;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Button btnFileDelete;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvFiles;
        private System.Windows.Forms.Button btnDir;
        private System.Windows.Forms.Label lbDir;
        private System.Windows.Forms.TextBox tbDir;
        private System.Windows.Forms.CheckBox cbSeparateFolders;


    }
}

